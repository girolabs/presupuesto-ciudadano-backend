from rest_framework import serializers
from .models import Actividad, Ministerio, Respuesta ,Orden


class MinisterioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ministerio
        fields = '__all__'


class ActividadSerializer(serializers.ModelSerializer):
    ministerio = MinisterioSerializer()
    class Meta:
        model= Actividad
        fields = '__all__'

    # def get_ministerio(self,obj):
    #     print(obj)
    #     ministerio = Ministerio.objects.filter(id=obj.id)
    #     print (MinisterioSerializer(ministerio).data)
    #     return MinisterioSerializer(ministerio).data

class OrdenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orden
        fields = ('orden','actividad')

    def create(self, validated_data):
        print('ola que tal')
        instance = super().create(validated_data)

class RespuestaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respuesta
        fields = ('ministerio','comentario')




class RespuestasSerializer(serializers.ModelSerializer):
    orden_set = OrdenSerializer(many=True)

    class Meta:
        model = Respuesta
        fields = ('ministerio','comentario','orden_set',)


    def create(self, validated_data):
        print('ola')
        print(validated_data)

        item_list = validated_data.pop('orden_set')
        print(item_list)
        instance = super().create(validated_data)
        instance.save()
        #print('Instancia',instance)
        for item in item_list:
            print('item interno')
            print(item)
            #Orden.objects.create(respuesta=instance,orden=item.orden)
            Orden( orden=item['orden'],respuesta=instance,actividad=Actividad.objects.get(id=item['actividad'].id)).save()
            #Orden(respuesta=instance,orden=item.orden,actividad=Actividad.objects.get(id=item.id)).save()
        return instance

class GraphDataSerializer(serializers.Serializer):
     orden = serializers.IntegerField()
     cantidad= serializers.IntegerField()

class ActividadesSerializer(serializers.Serializer):
    descripcion = serializers.CharField()
    data = GraphDataSerializer(many=True)

class MinisterioReportSerializer(serializers.Serializer):
    id = serializers.CharField()
    slug = serializers.CharField()
    nombre = serializers.CharField()
    actividades = ActividadesSerializer(many=True)








