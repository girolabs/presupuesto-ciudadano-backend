from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from rest_framework.views import APIView
from .serializers import ActividadSerializer, RespuestasSerializer,MinisterioReportSerializer,RespuestaSerializer
from .models import Actividad, Respuesta, Ministerio, Orden
from django_filters import rest_framework as filters


# Create your views here.


# class ActividadFilter(filters.FilterSet):
#     #descripcion = filters.CharFilter(field_name='descripcion', lookup_expr='icontains')
#
#     class Meta:
#         model = Actividad
#         fields = '__all__'




class ActividadesViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Actividad.objects.all()
    serializer_class = ActividadSerializer
    filter_fields = ('ministerio__slug','descripcion')

class RespuestaViewSet(viewsets.ModelViewSet):
    queryset = Respuesta.objects.all()
    serializer_class = RespuestasSerializer
    authentication_classes = []
    filter_fields = ('ministerio')


class RespuestaView(ListCreateAPIView):
    queryset = Respuesta.objects.all()
    serializer_class =RespuestasSerializer


class MinisterioReportView(APIView):
    def get(self,request):
        ministerios = []
        for m in Ministerio.objects.all():
            cant_actividades = m.actividad_set.count()
            actividades = []
            for a in m.actividad_set.all():
                descripcion = a.descripcion
                data = []

                for x in range(0,cant_actividades):
                    cantidad = Orden.objects.filter(actividad=a,orden=x).count()
                    data.append({'orden':x,'cantidad':cantidad})
                actividades.append({'descripcion':descripcion,'data':data})
            ministerios.append({'nombre':m.nombre,'slug':m.slug,'id':m.id,'actividades':actividades})
            serializer = MinisterioReportSerializer(ministerios,many=True)
            print(serializer.data)
        return Response(serializer.data)


class SetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 5


class RespuestasReport(ListAPIView):
    queryset = Respuesta.objects.all()
    serializer_class = RespuestaSerializer
    pagination_class = SetPagination
    filterset_fields = ['ministerio__slug',]
