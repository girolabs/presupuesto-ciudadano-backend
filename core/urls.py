from rest_framework import routers
from .views import  ActividadesViewSet, RespuestaViewSet,RespuestaView,MinisterioReportView,RespuestasReport
from django.urls import path

router = routers.SimpleRouter()
router.register('actividades',ActividadesViewSet)
#router.register('respuesta',RespuestaViewSet)

urlpatterns = [
    path('respuesta/', RespuestaView.as_view(), name='respuesta'),
    path('report/',MinisterioReportView.as_view(),name='reportMinisterio'),
    path('respuestas-report/',RespuestasReport.as_view(),name='respuestasReport')
    #path('orden/', OrdenView.as_view(), name='orden')
]


urlpatterns += router.urls
