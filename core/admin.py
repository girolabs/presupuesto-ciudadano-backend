from django.contrib import admin
from core.models import *
# Register your models here.

class ActividadAdmin(admin.ModelAdmin):
    list_filter = ('ministerio',)
    search_fields = ('descripcion',)


class ActividadInline(admin.StackedInline):
    model = Actividad


class MinisterioAdmin(admin.ModelAdmin):
    inlines = (ActividadInline,)


admin.site.register(Actividad, ActividadAdmin)
admin.site.register(Ministerio, MinisterioAdmin)
admin.site.register(Respuesta)
admin.site.register(Orden)
