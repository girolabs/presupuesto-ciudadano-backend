from django.db import models

# Create your models here.
class Ministerio(models.Model):
    nombre = models.CharField(max_length=50)
    slug = models.SlugField(max_length=40,unique=True)

    def __str__(self):
        return "%s" %(self.nombre)

class Actividad(models.Model):
    ministerio= models.ForeignKey(Ministerio, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=400, blank=True)

    def __str__(self):
        return "%s" %(self.descripcion)

class Respuesta(models.Model):
    comentario = models.CharField(max_length=200, blank=True)
    ministerio = models.ForeignKey(Ministerio, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" %(self.id)

class Orden(models.Model):
    actividad = models.ForeignKey(Actividad,on_delete=models.CASCADE)
    orden = models.PositiveIntegerField()
    respuesta = models.ForeignKey(Respuesta,on_delete=models.CASCADE)

    def __str__(self):
        return "%s" %(self.orden)